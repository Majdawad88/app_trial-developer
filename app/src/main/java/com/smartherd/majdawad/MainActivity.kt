package com.smartherd.majdawad

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnShowtoast.setOnClickListener {
            //code
            Log.i("MainActivity", "Button was clicked")
            //use logcat below to show the message, this one is used for the developer not the user

           Toast.makeText(this, "Button was Clicked", Toast.LENGTH_LONG).show()//will be shown in the emulator
        }

    }
}